-- LUALOCALS < ---------------------------------------------------------
local getmetatable, ipairs, loadstring, math, minetest, next, os,
      pairs, string, table, tonumber, type
    = getmetatable, ipairs, loadstring, math, minetest, next, os,
      pairs, string, table, tonumber, type
local math_ceil, math_floor, os_time, string_format, string_rep,
      string_sub, table_insert, table_sort
    = math.ceil, math.floor, os.time, string.format, string.rep,
      string.sub, table.insert, table.sort
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- SETTINGS

local conf = {}
local loadconfig
do
	local function confstr(suff)
		local str = minetest.settings:get(modname .. "_" .. suff)
		return str and str ~= "" and str or nil
	end
	local function confnum(suff)
		return tonumber(confstr(suff))
	end
	loadconfig = function()
		-- How often to publish updates to players. Too infrequent and the meter
		-- is no longer as "real-time", but too frequent and they'll get
		-- bombarded with HUD change packets.
		conf.interval = confnum("interval") or 2

		-- The total amount of time to include in the reporting period, in seconds.
		conf.report_period = confnum("report_period") or 60

		-- Size of buckets into which dtime values are sorted in weighted
		-- histogram.
		conf.bucket_step = confnum("bucket_step") or 0.05

		-- Maximum number of buckets. All step times too large for any other
		-- bucket will go into the highest bucket.
		conf.bucket_max = confnum("bucket_max") or 20

		-- Maximum number of characters to use for ascii bar graph
		conf.graphbar_width = confnum("graphbar_width") or 40

		-- Padding off screen corner.
		conf.padx = confnum("padx") or 8
		conf.pady = confnum("pady") or 24

		-- Separation between graph and summary
		conf.separate = confnum("separate") or 8

		-- Log level
		conf.loglv = confstr("loglevel") or "info"
	end
	loadconfig()
end

------------------------------------------------------------------------
-- MEASUREMENT

-- Queue of samples.
local samples = {}

-- Collect statistics at each step.
minetest.register_globalstep(function(dtime)
		samples[#samples + 1] = dtime
	end)

------------------------------------------------------------------------
-- PREFERENCES

-- Get lagometer settings per player
local function mode_get(player, meta)
	meta = meta or player:get_meta()
	local mget = function(k) return (meta:get_string(k) or "") ~= "" end
	local tbl = {
		prefer = {
			enable = mget("lagometer"),
			basic = mget("lagometer_basic"),
		}
	}
	local privs = minetest.get_player_privs(player:get_player_name())
	tbl.allow = {
		enable = tbl.prefer.enable and (privs.lagometer or privs.lagometer_basic),
		basic = tbl.prefer.basic or (not privs.lagometer)
	}
	return tbl
end

------------------------------------------------------------------------
-- HUD UPDATE

-- Lines to publish to HUDs
local graphlines = {}
local summarylines = {}

local updatehud
do
	-- Keep track of connected players and their meters.
	local meters = {}

	-- Remove meter registrations when players leave.
	minetest.register_on_leaveplayer(function(player)
			meters[player:get_player_name()] = nil
		end)

	local function deepeq(a, b)
		if type(a) == "table" and type(b) == "table" then
			for k, v in pairs(a) do
				if not deepeq(v, b[k]) then return end
			end
			for k, v in pairs(b) do
				if not deepeq(v, a[k]) then return end
			end
			return true
		end
		return a == b
	end

	-- Function to publish current HUD to a specific player.
	local hud_elem_type = minetest.features.hud_def_type_field and "type" or "hud_elem_type"
	updatehud = function(player)
		local pname = player:get_player_name()
		local hudlines = {}
		for i = 1, #graphlines do
			hudlines[#hudlines + 1] = {false, graphlines[i]}
		end
		for i = 1, #summarylines do
			hudlines[#hudlines + 1] = {true, summarylines[i]}
		end
		local mode = mode_get(player).allow
		for idx = 1, #hudlines do
			local line = hudlines[idx]

			local meter = meters[pname]
			if not meter then
				meter = {}
				meters[pname] = meter
			end
			local mline = meter[idx]

			-- Players with privilege will see the meter, players without
			-- will get an empty string. The meters are always left in place
			-- rather than added/removed for simplicity, and to make it easier
			-- to handle when the priv is granted/revoked while the player
			-- is connected.
			local text = ""
			if mode.enable and (line[1] or not mode.basic)
			then text = line[2] .. string_rep("\n", #hudlines - idx) end

			local def = {
				[hud_elem_type] = "text",
				position = {x = 1, y = 1},
				text = text,
				alignment = {x = -1, y = -1},
				number = 0xC0C0C0,
				offset = {x = -conf.padx, y = -conf.pady
					+ (line[1] and 0 or -conf.separate)},
				z_index = 2000,
			}

			if text ~= "" and not mline then
				meter[idx] = {
					def = def,
					hud = player:hud_add(def)
				}
			elseif mline and text == "" then
				player:hud_remove(mline.hud)
				meter[idx] = nil
			elseif mline then
				for k, v in pairs(def) do
					if not deepeq(v, mline.def[k]) then
						player:hud_change(mline.hud, k, v)
						mline.def[k] = v
					end
				end
			end
		end
	end
end

------------------------------------------------------------------------
-- REGISTER PRIVILEGES

do
	local function privupdated(name)
		local player = minetest.get_player_by_name(name)
		if player then return updatehud(player) end
	end

	-- Create a separate privilege for players to see the lagometer. This
	-- feature is too "internal" to show to all players unconditionally,
	-- but not so "internal" that it should depend on the "server" priv.
	minetest.register_privilege("lagometer", {
			description = "Can see the full lagometer",
			on_grant = privupdated,
			on_revoke = privupdated,
		})

	-- Create a separate privilege for players to see just the summary lines
	-- and not the full histogram. Hiding the histogram may have some utility
	-- in preventing players from overreporting lag due to psychological effects.
	minetest.register_privilege("lagometer_basic", {
			description = "Can see the lagometer summary",
			on_grant = privupdated,
			on_revoke = privupdated,
		})

	-- Display meter as soon as players join.
	minetest.register_on_joinplayer(updatehud)
end

------------------------------------------------------------------------
-- USER TOGGLE

do
	-- Change lagometer preferences and return correct message.
	local function mode_set(metakey)
		local function modestr(t)
			return (t.enable and "on" or "off") .. ":" .. (t.basic and "basic" or "full")
		end
		return function(pname)
			local player = minetest.get_player_by_name(pname)
			if not player then return end
			local meta = player:get_meta()
			local old = meta:get_string(metakey) or ""
			meta:set_string(metakey, (old == "") and "1" or "")
			updatehud(player)
			local mode = mode_get(player, meta)
			local prefer = modestr(mode.prefer)
			local allow = modestr(mode.allow)
			return true, "Lagometer mode set to " .. prefer
			.. (prefer == allow and "" or (", but limited to "
					.. allow .. " by privileges"))
		end
	end

	-- Command to manually toggle the lagometer.
	minetest.register_chatcommand("lagometer", {
			description = "Toggle the lagometer",
			func = mode_set("lagometer"),
		})

	-- Command to toggle between full and basic lagometer mode
	minetest.register_chatcommand("lagometer_mode", {
			description = "Toggle lagometer detail mode",
			func = mode_set("lagometer_basic"),
		})
end

------------------------------------------------------------------------
-- STATISTICS CYCLE

-- Full data to be published to backends outputs.
local fulldata

-- Update the HUDs and backend data.
local update
do
	-- Human-readable time format.
	local function timefmt(s)
		s = math_floor(s)
		if s < 60 then return string_format("%d", s) end
		local m = math_floor(s / 60)
		s = s - m * 60
		if m < 60 then return string_format("%d:%02d", m, s) end
		local h = math_floor(m / 60)
		m = m - h * 60
		if h < 24 then return string_format("%d:%02d:%02d", h, m, s) end
		local d = math_floor(h / 24)
		h = h - d * 24
		return string_format("%d.%02d:%02d:%02d", d, h, m, s)
	end

	-- Quickly count keys in a collection, plus special check to
	-- differentiate stepping vs non-stepping entities.
	local function countkeys(t, stepcount)
		local n = 0
		local s = 0
		local k = next(t)
		while k ~= nil do
			n = n + 1
			if stepcount and t[k].on_step then s = s + 1 end
			k = next(t, k)
		end
		return n, s
	end

	-- Pre-optimized things for histogram contsruction.
	local graphbar = string_rep("|", conf.graphbar_width)
	local meterfmt = "%2.2f %s %2.2f"
	local newhistogram = loadstring("return {" .. string_rep("0,", conf.bucket_max) .. "}")

	-- Get MT version once at startup, as it doesn't change.
	local mtversion = minetest.get_version()

	-- Function to update HUD lines.
	update = function()
		local max_lag = minetest.get_server_max_lag()
		local uptime = minetest.get_server_uptime()
		local connqty = countkeys(minetest.get_connected_players())
		local entqty, entstep = countkeys(minetest.luaentities, true)

		-- Prune old samples
		local total = 0
		for i = #samples, 2, -1 do -- i=1 would be no op
			total = total + samples[i]
			if total >= conf.report_period then
				local size = #samples - i + 1
				for j = 1, size do
					samples[j] = samples[j + i - 1]
				end
				for j = #samples, size + 1, -1 do
					samples[j] = nil
				end
				break
			end
		end

		-- Compute histogram and some basic statistics.
		local sampleqty = #samples
		local tsqr = 0
		local max = 0
		local histogram = newhistogram()
		for i = 1, sampleqty do
			local dtime = samples[i]
			tsqr = tsqr + (dtime * dtime)
			if dtime > max then max = dtime end
			local bucket = math_floor(dtime / conf.bucket_step) + 1
			if bucket > conf.bucket_max then bucket = conf.bucket_max end
			histogram[bucket] = histogram[bucket] + dtime
		end

		-- Construct the weighted histogram visualization.
		graphlines = {}
		for bucket = 1, conf.bucket_max do
			local qty = histogram[bucket]
			local size = bucket * conf.bucket_step
			table_insert(graphlines, 1, qty <= 0 and "" or string_format(meterfmt, qty,
					-- Maximum width of a graph bar corresponds to 50% of the total
					-- time in the window, so that there will never be 2 bars of the
					-- same length that don't have the same amount of time, even if
					-- there is one bar that's longer than all others and is cut off.
					string_sub(graphbar, 1, math_ceil(qty * 2 * conf.graphbar_width
							/ conf.report_period)), size))
		end

		-- Compute quantiles.
		local quants
		if sampleqty >= 1 then
			local t = {}
			for i = 1, sampleqty do t[i] = samples[i] end
			table_sort(t)
			quants = {0, 0, 0, 0, 0}
			local accum = 0
			for i = 1, sampleqty do
				local ti = t[i]
				local idx = math_ceil(accum / total * 4 + 1)
				quants[idx] = ti
				accum = accum + ti
			end
		end

		-- Construct final summary HUD lines.
		summarylines = {}
		local quantstr = quants and string_format("%0.2f %0.2f %0.2f %0.2f %0.2f",
			quants[1], quants[2], quants[3], quants[4], quants[5]) or "?"
		summarylines[#summarylines + 1] = string_format("avg %0.3f mtbs %0.3f quants %s",
			total / sampleqty, tsqr / total, quantstr)
		summarylines[#summarylines + 1] = string_format(
			"v%s con %d ent %d es %d max_lag %0.3f uptime %s",
			mtversion.hash or mtversion.string, connqty, entqty,
			entstep, max_lag, timefmt(uptime))

		-- Publish full data to backends.
		fulldata = {
			timestamp = os_time(),
			conf = conf,
			mtversion = mtversion,
			uptime = uptime,
			connections = connqty,
			entities = entqty,
			entity_on_step = entstep,
			max_lag = max_lag,
			total = total,
			sampleqty = sampleqty,
			mean = total / sampleqty,
			mtbs = tsqr / total,
			quants = quants,
			histogram = histogram,
		}
	end
end

local publish
do
	local nextpub = {}
	local function publishtimer(key, default, func)
		local rawconf = minetest.settings:get(modname .. "_publish_" .. key)
		local pubint = tonumber(rawconf) or default
		if (not pubint) or (pubint <= 0) then return end
		local ts = fulldata.timestamp
		local np = nextpub[key]
		if not np then
			nextpub[key] = ts + pubint
			return
		end
		if ts < np then return end
		np = np + pubint
		if np < ts then np = ts end
		nextpub[key] = np
		return func()
	end
	publish = function()
		-- Apply the appropriate text to each meter.
		for _, player in ipairs(minetest.get_connected_players()) do
			updatehud(player)
		end

		-- Write to system log.
		publishtimer("log", 60, function()
				minetest.log(conf.loglv, modname .. ": "
					.. minetest.write_json(fulldata))
			end)

		-- Publish a JSON dump for external use.
		publishtimer("json", 0, function()
				minetest.safe_file_write(
					minetest.get_worldpath() .. "/lagometer.json",
					minetest.write_json(fulldata))
			end)
	end
end

-- Run everything on a cycle.
do
	local function updatecycle()
		loadconfig()
		update()
		publish()
		minetest.after(conf.interval, updatecycle)
	end
	minetest.after(conf.interval, updatecycle)
end

-- On changing a setting, reload config and republish.
do
	local function reloadand(...)
		loadconfig()
		if fulldata then publish() end
		return ...
	end
	local meta = getmetatable(minetest.settings)
	for k, v in pairs(meta) do
		if string_sub(k, 1, 3) == "set" then
			meta[k] = function(...)
				return reloadand(v(...))
			end
		end
	end
end
