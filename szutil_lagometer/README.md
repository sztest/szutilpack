Display a full near-realtime analysis of server "lag", i.e. globalstep dtimes.  This allows admins to monitor system performance, or mod authors to monitor code performance impact, continuously, instead of having to do /status over and over.

The lagometer also provides much more useful statistics compared to minetest's max_lag, and gives you an idea of how often lag spikes are happening and how much total impact they have.

The appearance of the lagometer to each player is controlled by:

- The `lagometer` privilege: users who have this can access all modes of the lagometer.
- The `lagometer_basic` privilege: users who have this, but don't have the "lagometer" one, can only see the summary statistics, not the histogram.
- The `/lagometer` chat command: this toggles whether the user wants to see the lagometer at all or not.
- The `/lagometer_mode` chat command: this toggles between "full" view with hisogram, and "basic" view with only the summary lines.

Note that users can toggle their preferences freely at any time; they don't need to have the corresponding privileges to change a preference, their preferences are merely filtered by the privileges.

The "full" lagometer includes a weighted histogram visualization of all steps over the sampling period.  Each bar represents the total amount of time spent inside globalsteps of a length up to the bucket size, but larger than all smaller buckets.  The top bucket captures also captures all time that doesn't fit into a bucket at all.  This visualization makes it easier to see periodic behavior, like bimodal distributions with a lot of 0.05 and 0.25 steps caused by ABM saturation.

Additionally, there are some of lines of information-dense summary statistics at the bottom, with:

- `avg`: Average size of steps, averaged across steps.
- `mtbs`: Mean time between steps, averaged across time; this provides a much better sense of the impact of large outliers than "avg".
- `quants`: Quantiles over sampling period, consisting of: minimum, 1st quartile, median, 3rd quartile, max.  These are a coarse summary of the histogram data.
- `v`: Minetest engine version.
- `con`: Number of currently active player connections.
- `ent`: Number of currently active luaentities.
- `es`: Number of luaentities that have an on_step callback.
- `max_lag`: The engine's exponentially-averaged max_lag statistic.  This statistic is fairly useless (quantile max is more accurate) but it's what's used in e.g. the server list.
- `uptime`: How long the server has been running continuously.
