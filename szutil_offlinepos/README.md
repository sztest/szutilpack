Keep track of player last position, even when players are offline. Add support for some commands utilizing player positions.

- Support `/teleport <player>` to offline players.
- Add `/pos <player>` to get player's current position.
- Add `/postrack <pattern>` and `/posuntrack <pattern>` to enable/disable player position waypoint HUDs.

------------------------------------------------------------------------
