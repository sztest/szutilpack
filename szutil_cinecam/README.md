This mod provides a more "cinematic" camera positioning system than szutil_watch, providing a series of 3rd-party "chase" cams that move via simple jumpcuts.

When no players are online, the camera will switch to a mode where it watches the "spawn" area, or rotating through a set of waypoints that can be managed via chatcommands at runtime.

WARNING: this mod is intentionally missing a lot of functionality that you would expect, like rendering the player invisible, preventing the player from falling or taking damage, etc.  This is the responsibility of other mods.  See szutil_stealth for the invisibility feature; the rest will be game-depenent.
