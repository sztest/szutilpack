Allow users with special `logtrace` priv to watch traces of server logs, useful for admin or debugging purposes.

Use the `/logtrace` command to get/set the logging level.  The special `all` and `none` levels capture all and none, respectively, while the other levels capture each different minetest standard logging level and anything higher.  If a message cannot be classified (it doesn't have a valid logging level) then it will only be in the "all" feed.
