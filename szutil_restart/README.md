This mod manages the graceful shutdown (i.e. restart, given that external service management auto-restarts it) based on a number of potential triggers:

- An external signal, by creating a file in the world path, that can be used by external scripts to trigger a restart, e.g. if the game/mods have been updated.
- Optionally, the server has been up and running for a configured maximum amount of time.
- Optionally, the server has become empty, after players have been connected to it, to force unloading mapblocks and conserve RAM while the server is idle.

Shutdowns are inhibited by players being on the server (unless they have a special privilege).  If players are connected at the time a restart is requested, they will be given a countdown.  If the server has been up for less than a configurable minimum amount of time, the countdown will last until that uptime has been reached, or else they will be given a standard grace period from the time of the trigger.  If no player is present on the server (or all players leave) then the rest of the countdown is skipped and the server restarts immediately.

The mod also provides restart warnings and countdowns via the chat stream (esp. for external chat bridges using szutil_chatsocket), a HUD (for the final portion of the countdown) and in the /status line.  If no players are online at the time a restart is triggered, it will be silent, to avoid creating noise on chat bridges.

This mod is mainly intended for restarts, not "maintenance window" shutdowns where the server is intended to stay down for a while.  In particular:

- External scripts must provide the restart after shutdown.
- Because players have the ability to cancel the countdown and shutdown early, it cannot be coordinated with external events that are scheduled by other means, e.g. the shutdown and restart of other MT servers, or timed cronjobs.