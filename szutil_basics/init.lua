-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, pairs, string, table
    = ipairs, minetest, pairs, string, table
local string_format, table_concat, table_remove, table_sort
    = string.format, table.concat, table.remove, table.sort
-- LUALOCALS > ---------------------------------------------------------

-- Chat command to completely destroy a player account, including auth.
minetest.register_chatcommand("destroy_player", {
		params = "<playername>",
		description = "Completely destroy a player account",
		privs = {privs = true},
		func = function(name, param)
			if not minetest.player_exists(param) then
				return false, "Player not found"
			end
			if minetest.check_player_privs(param, "privs") then
				return false, "Cannot delete an admin account"
			end
			if minetest.get_player_by_name(param) then
				return false, "Cannot delete online players"
			end
			minetest.remove_player(param)
			minetest.remove_player_auth(param)
			minetest.log("warning", string_format(
					"player account %q was destroyed by %q",
					param, name))
			return true, string_format("Player %q destroyed", param)
		end
	})

-- Chat command to write to the server log.
do
	local allowed = {"verbose", "info", "action", "warning", "error"}
	local allowed_idx = {}
	for _, v in ipairs(allowed) do allowed_idx[v] = true end
	local allowed_str = table_concat(allowed, ", ")
	minetest.register_chatcommand("log", {
			params = "<severity> <message>",
			description = "Write any message to server log",
			privs = {server = true},
			func = function(name, param)
				local words = param:split(" ")
				local severity = words[1]
				table_remove(words, 1)
				if not allowed_idx[severity] then
					return false, "severity must be one of " .. allowed_str
				end
				minetest.log("action", name .. " used the log command")
				minetest.log(severity, table_concat(words, " "))
			end
		})
end

-- Chat command to analyze entities
minetest.register_chatcommand("luaentities", {
		description = "Summary of luaentities by type",
		privs = {["debug"] = true},
		func = function()
			local sum = {}
			local keys = {}
			for _, ent in pairs(minetest.luaentities) do
				local n = ent.name or "?"
				if not sum[n] then
					sum[n] = 1
					keys[#keys + 1] = n
				else
					sum[n] = sum[n] + 1
				end
			end
			table_sort(keys, function(a, b)
					local sa = sum[a]
					local sb = sum[b]
					return sa == sb and a < b or sa > sb
				end)
			for i = 1, #keys do
				local k = keys[i]
				keys[i] = string_format("%q=%d", k, sum[k])
			end
			return true, table_concat(keys, ", ")
		end
	})

-- Moderators and admins can always bypass player limits,
-- important for emergency access.
minetest.register_can_bypass_userlimit(function(name)
		local privs = minetest.get_player_privs(name)
		return privs.privs or privs.basic_privs
	end)
