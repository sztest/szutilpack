This mod is a dumping ground for minute features that are too small to warrant their own mod, are obvious features that should probably have just been included in base, and are limited to certain audiences (admins and moderators) and so are generally safe to enable always.

Currently includes:

- Adds a `/destroy_player` command that completely deletes a player's account, including auth info.  This can be useful for some games like e.g. NodeCore, where player "ghosts" are cleaned up and their items drop if the account is destroyed, or just for general cleanup.
- Moderators and admins (privs/basic_privs priv owners) can always log in and bypass the user limit, making it still possible for admins to access the server during a botting attack.
