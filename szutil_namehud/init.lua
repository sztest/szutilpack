-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, rawget, rawset, string, table, tonumber
    = minetest, pairs, rawget, rawset, string, table, tonumber
local string_rep, table_sort
    = string.rep, table.sort
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local function configxy(name, defx, defy)
	return {
		x = tonumber(minetest.settings:get(modname .. "_" .. name .. "_x")) or defx,
		y = tonumber(minetest.settings:get(modname .. "_" .. name .. "_y")) or defy
	}
end
local hud_pos = configxy("pos", 0, 1)
local hud_align = configxy("align", 1, -1)
local hud_offs = configxy("offs", 8, -24)

local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

function api.get_hud_def(player)
	return {
		number = 0xe0e0e0,
		text = player:get_player_name()
	}
end

local function lazygetnames()
	local cached
	return function()
		if cached then return cached end
		cached = {}
		for _, player in pairs(minetest.get_connected_players()) do
			if not minetest.check_player_privs(player, "stealth") then
				cached[#cached + 1] = api.get_hud_def(player)
			end
		end
		table_sort(cached, function(a, b) return a.text:upper() < b.text:upper() end)
		return cached
	end
end

local cached_huds = {}
minetest.register_on_leaveplayer(function(player)
		cached_huds[player:get_player_name()] = nil
	end)

local hud_elem_type = minetest.features.hud_def_type_field and "type" or "hud_elem_type"
local function sethuds(player, names)
	local pname = player:get_player_name()
	local phud = cached_huds[pname]
	if not phud then
		phud = {}
		cached_huds[pname] = phud
	end
	for i = 1, #names do
		local n = names[i]
		local text = string_rep("\n", i - 1) .. n.text .. string_rep("\n", #names - i)
		local h = phud[i]
		if h then
			if h.text ~= text then
				player:hud_change(h.id, "text", text)
				h.text = text
			end
			if h.number ~= n.number then
				player:hud_change(h.id, "number", n.number)
				h.number = n.number
			end
		else
			phud[i] = {
				text = text,
				id = player:hud_add({
						[hud_elem_type] = "text",
						position = hud_pos,
						text = text,
						number = n.number,
						alignment = hud_align,
						offset = hud_offs,
						z_index = -300,
					})
			}
		end
	end
	for i = #names + 1, #phud do
		player:hud_remove(phud[i].id)
		phud[i] = nil
	end
end

local function update()
	local getnames = lazygetnames()
	for _, player in pairs(minetest.get_connected_players()) do
		sethuds(player, (player:get_meta():get_string(modname) ~= "")
			and getnames() or {})
	end
end

minetest.register_chatcommand("names", {
		description = "Toggle online player HUD",
		func = function(name)
			local player = minetest.get_player_by_name(name)
			if not player then return false, "must be online" end
			local meta = player:get_meta()
			local val = (meta:get_string(modname) ~= "") and "" or "1"
			meta:set_string(modname, val)
			return update()
		end
	})

minetest.register_on_joinplayer(update)
minetest.register_on_leaveplayer(function() return minetest.after(0, update) end)
minetest.after(0, update)
