This adds a HUD that shows player's input controls.  This is especially useful for livestreaming or demo videos, where you might want to show players what exact set of control inputs is used to execute some specific action.

It's run by the server, so there may be some latency; it performs best in single player or LAN games.

Use `/controhud #` to set the size scale to #, where # is a number of value zero or greater.  A value of zero hides the HUD.
