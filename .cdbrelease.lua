-- LUALOCALS < ---------------------------------------------------------
local dofile, io, ipairs, string, table
    = dofile, io, ipairs, string, table
local io_open, io_popen, string_gsub, string_match, table_concat,
      table_sort
    = io.open, io.popen, string.gsub, string.match, table.concat,
      table.sort
-- LUALOCALS > ---------------------------------------------------------

-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

local version = dofile("./version.lua")

local metadata = {
	pkg = "szutilpack",
	version = version,
	type = "mod",
	dev_state = "ACTIVELY_DEVELOPED",
	title = "SzUtilPack",
	short_description = "A collection of misc dependency-free utilities primarily for server hosts",
	tags = {
		"complex_installation",
		"environment",
		"library",
		"world_tools",
		"player_effects",
		"server_tools",
		"transport"
	},
	content_warnings = {},
	license = "MIT",
	media_license = "MIT",
	repo = "https://gitlab.com/sztest/szutilpack",
	maintainers = {"Warr1024"},
	screenshots = {readbinary('.cdbscreen.webp')}
}

local function exist(file)
	local f = io_open(file, "rb")
	if f then f:close() end
	return f ~= nil
end

local mods = {}
do
	local f = io_popen("ls -A -1 .")
	while true do
		local l = f:read("*line")
		if not l then break end
		if exist(l .. "/mod.conf") then
			mods[#mods + 1] = l
		end
	end
	table_sort(mods)
end

local parts = {
	readtext("README.md"),
	"",
	readtext(".cdb-readme-pre.md"),
	""
}

local commitid = "master"
if string_match(version, "-") then
	commitid = string_gsub(version, ".*-", "")
end

local function getmodlink(name)
	for _, fn in ipairs({"README.md", "README.txt", "README"}) do
		if exist(name .. "/" .. fn) then
			return "[" .. name .. "](https://gitlab.com/sztest/szutilpack/-/blob/"
			.. commitid .. "/" .. name .. "/" .. fn .. ")"
		end
	end
	return "**" .. name .. "**"
end

local function scanmods(reqsock)
	for _, m in ipairs(mods) do
		if reqsock == exist(m .. "/.requires-luasockets") then
			local conf = readtext(m .. "/mod.conf")
			local desc = string_match(conf, "description%s*=%s*(.-)\n")
			parts[#parts + 1] = "- " .. getmodlink(m) .. ": " .. desc
		end
	end
end

scanmods(false)
parts[#parts + 1] = ""
parts[#parts + 1] = readtext(".cdb-readme-sock.md")
parts[#parts + 1] = ""
scanmods(true)
parts[#parts + 1] = ""
parts[#parts + 1] = readtext(".cdb-readme-post.md")

metadata.long_description = string_gsub(table_concat(parts, "\n"), "\n\n+", "\n\n")
return metadata

-- luacheck: pop
