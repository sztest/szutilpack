Sometimes `player:set_pos(pos)` (a.k.a. a teleport) will simply fail (especially for high-latency players) and the player will be left standing where they are.  For some game mechanics, where triggering the teleportation is expensive, this is unacceptable.  This mod tries to correct for that by retrying the teleportation until it succeeds.

Configuration:
- `szutil_teleportfix_speed` default `50`- maximum reasonable expected player speed outside of teleports, in m/s
- `szutil_teleportfix_maxtime` default `60` - maximum amount of time to "watch" a teleport for regressions, in seconds
- `szutil_teleportfix_interval` default `0.5`- minimum time between teleportation retries, in seconds (default 0.5)
- `szutil_teleportfix_mindist` default `2` - minimum distance for a set_pos to be considered a "teleport" that may need to be retried