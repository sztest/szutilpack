This mod kicks players who have been idle (no evidence of any input for a certain amount of time) automatically.  This can be used to prevent players from idling on a server and using resources, or to automatically disconnect connected devices so players can hop onto a different device even if they left the original device unattended but running.

The timeout is controlled by the `szutil_idlekick_timeout` setting, in seconds, defaulting to 600 (10 minutes).  The kick reason message can be customized via `szutil_idlekick_reason`.

If the `szutil_idlekick_invert` setting is false, then all players are subject to idle timeout kicks, except those with the `szutil_idlekick` privilege.  If true, then only those with the `szutil_idlekick` priv are automatically kicked.
