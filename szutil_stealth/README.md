This mod tries to make a player completely invisible, including in-game model, nametag, muting join/leave messages, and not displaying the name in the status line.  This can be used for administering a server or spectating the action without accidentally affecting player behavior or disrupting the normal flow of things.

Currently the "stealth" attribute is managed by the priv `stealth`.  Granting the priv does not change the player state in realtime; a re-login may be necessary.

Players cannot currently toggle stealth voluntarily.  Note that toggling the priv while the affected player is logged in may cause login/logout messages to mismatch, revealing the use of stealth.

Players who have both stealth and privs will have `interact` and `shout` revoked automatically on login to prevent revealing the stealth user's presence by accidentally interacting with the environment or chat; use `/grantme` each time interact privs are necessary.  Players without the `privs` priv will not have interact revoked automatically (including those with `basic_privs`), since the primary use-case for this is for full admins and the exact meaning of the basic_privs priv can change.
