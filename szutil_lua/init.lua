-- LUALOCALS < ---------------------------------------------------------
local dump, loadstring, minetest, pcall, table, tostring
    = dump, loadstring, minetest, pcall, table, tostring
local table_remove
    = table.remove
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

minetest.register_privilege(modname, {
		description = "Can run arbitrary lua code",
		give_to_singleplayer = false,
		give_to_admin = false,
	})

minetest.register_chatcommand("lua", {
		privs = {[modname] = true},
		description = "Run arbitrary lua code",
		func = function(_, param)
			local func, synerr = loadstring(param)
			if not func then
				return false, "parse error: " .. tostring(synerr)
			end
			local result = {pcall(func)}
			if not result[1] then
				return false, "runtime error: " .. tostring(result[2])
			end
			table_remove(result, 1)
			return true, "returned: " .. dump(result)
		end
	})
