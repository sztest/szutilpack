Adds a `/lua` command to allow an admin to execute any arbitrary lua in the server context that they want.

This is comparable to the //lua command functionality present in worldedit, but can be enabled without all the other worldedit stuff.

The code is run inside a synchronous protected call, with no input parameters.  The result (return or error) is sent to the user.