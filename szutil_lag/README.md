Adds a simple `/lag <seconds>` command for anyone with the `server` priv.  When set to anything above zero, no server step will be allowed to complete in less than that amount of time (it effectively set the minimum lag per step).  CPU cycles will be wasted to meet the lag requirement (it's a busy-wait).

This is useful for quickly testing lag-dependent issues such as machines jamming under high lag conditions.

Lag is not preserved across server restarts and is reset to zero.

There is a maximum safe lag level assigned at runtime, which can be overridden by a setting, but changing the setting requires a restart.
