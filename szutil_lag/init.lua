-- LUALOCALS < ---------------------------------------------------------
local minetest, string, tonumber
    = minetest, string, tonumber
local string_format
    = string.format
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local max = tonumber(minetest.settings:get(modname .. "_max")) or 2

local lag = 0
local last = minetest.get_us_time()
minetest.register_globalstep(function()
		if lag <= 0 then return end
		local exp = last + lag * 1000000
		while minetest.get_us_time() < exp do end
		last = exp
	end)

minetest.register_chatcommand("lag", {
		description = "Set server minimum lag amount",
		params = "<seconds_per_step>",
		privs = {server = true},
		func = function(_, param)
			param = tonumber(param)
			if not param then return false, "invalid number" end
			if param < 0 then param = 0 end
			if param > max then param = max end
			lag = param
			return true, string_format("Lag set to at least %0.3fs per step", lag)
		end
	})
