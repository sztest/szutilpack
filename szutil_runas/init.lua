-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, pcall, string, table
    = ipairs, minetest, pcall, string, table
local string_sub, table_concat
    = string.sub, table.concat
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

minetest.register_privilege(modname, {
		description = "User may use the runas command",
		give_to_admin = true,
		give_to_singleplayer = false
	})
minetest.register_privilege(modname .. "_protect", {
		description = "No other users may run a command as this user",
		give_to_admin = true,
		give_to_singleplayer = false
	})

-- Duplicate command reponses to the user actually issuing
-- the command, during the command's runtime.
local msgdupe
do
	local old_chatsend = minetest.chat_send_player
	function minetest.chat_send_player(who, text, ...)
		if msgdupe and who == msgdupe.from then
			old_chatsend(msgdupe.to, text, ...)
		end
		return old_chatsend(who, text, ...)
	end
end

local function runas_core(runner, runee, password, command)
	if not minetest.check_player_privs(runner, modname)
	or minetest.check_player_privs(runee, modname .. "_protect")
	then return false, "access denied" end

	local authdata = minetest.get_auth_handler().get_auth(runee)
	if not (authdata and authdata.password
		and minetest.check_password_entry(runee, authdata.password, password))
	then return false, "access denied" end

	if string_sub(command, 1, 1) ~= "/" then command = "/" .. command end

	msgdupe = {from = runee, to = runner}
	for _, v in ipairs(minetest.registered_on_chat_messages) do
		local ok, err = pcall(function() return v(runee, command) end)
		if ok and err then return end
		if not ok then
			return minetest.chat_send_player(runee, err)
		end
	end
	minetest.chat_send_player(runee, "unrecognized command")
end

minetest.register_chatcommand("runas", {
		description = "Run a command as another user",
		params = "<user> <password> <command> [params...]",
		privs = {[modname] = true},
		func = function(name, params)
			params = params:split(" ")
			local passwd = {}
			for i = 2, #params do
				if string_sub(params[i], -1) ~= "\\" then
					passwd[#passwd + 1] = params[i]
					local cmd = {}
					for j = i + 1, #params do
						cmd[#cmd + 1] = params[j]
					end
					return runas_core(name, params[1],
						table_concat(passwd, " "),
						table_concat(cmd, " "))
				end
				passwd[#passwd + 1] = string_sub(params[i], 1, -2)
			end
			return false, "syntax error"
		end
	})
