This mod allows a user to "log in as" another user, using their password, assuming their identity, privileges, and other limitations, and executing a command as them.  This is ideally meant to be used with szutil_consocket, to allow e.g. attaching a web interface to allow non-full-admin moderators to issue moderation commands via web when they cannot access the game directly.

The `/runas` command takes parameters:

- the user to run as (no spaces allowed)
- that user's password (escape spaces with a backslash)
- the command to run (spaces allowed)

This mod only supports slash-commands, and will add a slash to the beginning of any command missing one; it does not allow chat impersonation (unless another mod adds a command for that).

---

**WARNING**: Chat commands (including the runas password) are transmitted over the wire in plaintext and can be read by MITM or passive listeners. Using normal player login (which uses Secure Remote Passwords) or using szutil_consocket over SSH is safer.
