This mod automatically prunes old players who have not joined the world in a long enough time.

Old players may clutter up and bog down a world, especially if mods are in use that deal with offline players.

Players are protected from deletion if any of the following are true:

- They have last logged in within the required timeframe
- They are currently connected to the server
- They have the "noprune" priv
- They are singleplayer or the configured admin

The following settings control operation:

- `szutil_pruneplayers_interval` (default 3600) - Seconds between automatic prune scans
- `szutil_pruneplayers_maxdays` (default 90) - Minimum number of days since last login for a player to be eligible to be pruned
- `szutil_pruneplayers_keepauth` (default false) - If true, only delete the player data, but do not wipe the account registration/auth/privileges; if false, completely purge the account.

Pruning scans are done lackadaisically in the background to minimize disruption to normal server operations.
