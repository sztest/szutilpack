-- LUALOCALS < ---------------------------------------------------------
local ipairs, math, minetest, os, string, tonumber
    = ipairs, math, minetest, os, string, tonumber
local math_random, os_time, string_format
    = math.random, os.time, string.format
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local modstore = minetest.get_mod_storage()

local scaninterval = tonumber(minetest.settings:get(modname .. "_interval")) or 3600
local maxdays = tonumber(minetest.settings:get(modname .. "_maxdays")) or 90
local keepauth = minetest.settings:get_bool(modname .. "_keepauth")

local protectnames = {
	singleplayer = true,
	[minetest.settings:get("name")] = true,
}

local noprune = "noprune"
minetest.register_privilege(noprune, {
		description = "Do not automatically prune player for unuse",
		give_to_admin = false,
		give_to_singleplayer = false,
	})

------------------------------------------------------------------------

local queue_current
local queue_next
local queue_pos = 1

local function processqueue()
	if queue_current then
		local act = queue_current[queue_pos]
		act()
		queue_pos = queue_pos + 1
		if queue_pos > #queue_current then
			queue_current = nil
			queue_pos = 1
		end
	end
	if not queue_current then
		queue_current = queue_next
		queue_next = nil
	end
	if queue_current then
		minetest.after(0, processqueue)
	end
end

local function enqueue(act)
	queue_next = queue_next or {}
	queue_next[#queue_next + 1] = act
	if not queue_current then
		queue_current = queue_next
		queue_next = nil
		minetest.after(0, processqueue)
	end
end

------------------------------------------------------------------------

local cache = {}
do
	local function bump(pname)
		local t = os_time()
		cache[pname] = t
		modstore:set_string(pname, string_format("%d", t))
	end
	minetest.register_on_authplayer(function(pname, _, success)
			if success then bump(pname) end
		end)
	minetest.register_on_joinplayer(function(player)
			bump(player:get_player_name())
		end)
	minetest.register_on_authplayer(function(name, _, success)
			if not success then return end
			bump(name)
		end)
	minetest.register_on_leaveplayer(function(player)
			bump(player:get_player_name())
		end)
	local function scanall()
		for _, player in ipairs(minetest.get_connected_players()) do
			bump(player:get_player_name())
		end
	end
	minetest.register_on_shutdown(scanall)
	local function bgscan()
		minetest.after(1, bgscan)
		return scanall()
	end
	minetest.after(0, bgscan)
end

------------------------------------------------------------------------

local function check(pname)
	if protectnames[pname] or minetest.get_player_by_name(pname) then return end

	local handler = minetest.get_auth_handler()
	local data = handler.get_auth(pname)
	if not data then
		minetest.log("info", string_format(
				"%s gc %s", modname, pname))
		cache[pname] = nil
		modstore:set_string(pname, "")
		return
	end

	if data.privileges[noprune] then return end

	local logintime = data.last_login
	local seentime = tonumber(cache[pname] or modstore:get_string(pname)) or 0
	local seen = (logintime > seentime) and logintime or seentime

	local now = os_time()
	local daysago = (now - seen) / 86400
	if daysago <= maxdays then return end

	cache[pname] = nil
	modstore:set_string(pname, "")
	minetest.log("action", string_format(
			"Deleting player%s %q last seen %0.1f day(s) ago",
			keepauth and "" or " + auth", pname, daysago))
	minetest.remove_player(pname)
	if not keepauth then minetest.remove_player_auth(pname) end
end

local function startscan()
	local handler = minetest.get_auth_handler()
	local seen = {}
	local names = {}
	for name in handler.iterate() do
		if not seen[name] then
			seen[name] = true
			names[#names + 1] = name
		end
	end
	for _, name in ipairs(modstore:get_keys()) do
		if not seen[name] then
			seen[name] = true
			names[#names + 1] = name
		end
	end
	for i = #names, 2, -1 do
		local j = math_random(1, i)
		local x = names[i]
		names[i] = names[j]
		names[j] = x
	end
	for i = 1, #names do
		local pname = names[i]
		enqueue(function() return check(pname) end)
	end
	enqueue(function()
			minetest.after(
				scaninterval * (math_random() + 0.5),

				startscan)
		end)
end

minetest.after(0, startscan)
