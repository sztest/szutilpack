A simple mod for granting players privs based on "experience points" accumulated through playing.

Players are granted experience points each time an action is carried out based on the value assigned to the `szutil_xplevel_xp_<ACTION>` setting for each ACTION:

- See the PLAYER ACTIVITY EVENT HOOKS section of init.lua for the valid ACTION values for single actions.
- For actions "move" and "idle", points values are multiplied by the number of seconds doing each action.

Players can be granted privs, based on the `szutil_xplevel_privs` setting, which is in the format of `<xp1>=<priv1a>,<priv1b>;<xp2>=<priv2>...` When a player has >= xp1 experience points (number) then they will be granted priv1a and priv1b, when they reach xp2, they will be granted priv2, etc.

External mods can query the xp level of a player via the public API: `minetest.get_szutil_xplevel(player or name)`

Players can query the xp level of a person using the `/szutil_xplevel [player]` command.
